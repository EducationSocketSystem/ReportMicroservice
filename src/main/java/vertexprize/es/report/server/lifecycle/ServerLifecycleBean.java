/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vertexprize.es.report.server.lifecycle;

import io.quarkus.runtime.StartupEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import org.jboss.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import vertexprize.es.core.objectcontainer.ObjectContainer;
import vertexprize.es.core.report.AbstractReport;
import vertexprize.es.core.report.ReportBody;
import vertexprize.es.core.report.ReportFooter;
import vertexprize.es.core.report.ReportHead;
import vertexprize.es.core.report.ReportTest;
import vertexprize.es.core.report.field.AbstractField;
import vertexprize.es.core.report.field.FieldNumberDouble;
import vertexprize.es.core.report.field.FieldText;
import vertexprize.es.core.report.field.address.FieldAddressInteger;
import vertexprize.es.core.report.table.ReportTable;
import vertexprize.es.report.server.service.ReportService;

/**
 * Класс, который описывает события микросервиса.
 * 
 * @author Safronov.O.V
 */
@ApplicationScoped
public class ServerLifecycleBean {

    /**
     * Логгер из пакета org.jboss.logging.Logger.
     */
    private static final Logger log = Logger.getLogger(ServerLifecycleBean.class.getName());

    /**
     * Экземпляр класса reportService, 
     * при помощи которого происходит взаимодествие с базой данных.
     */
    @Inject
    ReportService reportService;

    /**
     * Действия, которые происходят при старте сервера.
     * 
     * @param event Старт сервера
     */
    void onStart(@Observes StartupEvent event) {

        log.info("[" + event.toString() + "] Сервер ReportMicroservice. Старт сервера ... ");

        log.info("Инициализация базы данных Record с использованием тестовых записей ...");
        log.info("Создание экземпляров класса ReportTest ...");
        ReportTest report1 = new ReportTest();
        this.createReport(report1, "Test1");
        ReportTest report2 = new ReportTest();
        this.createReport(report2, "Test2");
        ReportTest report3 = new ReportTest();
        this.createReport(report3, "Test3");
        ReportTest report4 = new ReportTest();
        this.createReport(report4, "Test4");
        ReportTest report5 = new ReportTest();
        this.createReport(report5, "Test5");
        ReportTest report6 = new ReportTest();
        this.createReport(report6, "Test6");
        ReportTest report7 = new ReportTest();
        this.createReport(report7, "Test7");

        log.info("Получение списка всех отчетов ...");
        ObjectContainer containerGetAll = reportService.getAllReports();
        log.info(containerGetAll.printInfos() + "");
        if (!containerGetAll.getWarnings().isEmpty()) {
            log.info(containerGetAll.printWarnings());
        }
        if (!containerGetAll.getErrors().isEmpty()) {
            log.info(containerGetAll.printErrors());
        }

        log.info("Получение отчета с id = e031d228-cdaa-4934-8dec-8ef131163daf ...");
        ObjectContainer<AbstractReport> containerReport = reportService.getReport(UUID.fromString("e031d228-cdaa-4934-8dec-8ef131163daf"));
        log.info(containerReport.printInfos());
        if (!containerReport.getWarnings().isEmpty()) {
            log.info(containerReport.printWarnings());
        }
        if (!containerReport.getErrors().isEmpty()) {
            log.info(containerReport.printErrors());
        }

        
        log.info("Вызов метода writeXlsxReport");
        ObjectContainer containerReadXlsxReport = report1.writeXlsxReport("/home/user/Рабочий стол", "Test1-2023-04-14.xlsx", "Test1");
        log.info("Открытие сообщений из containerReadXlsxReport");
        log.info(containerReadXlsxReport.printInfos());
        if (!containerReadXlsxReport.getWarnings().isEmpty()) {
            log.info(containerReadXlsxReport.printWarnings());
        }
        if (!containerReadXlsxReport.getErrors().isEmpty()) {
            log.info(containerReadXlsxReport.printErrors());
        }        
        
//        log.info("Вызов метода writeExcelReport");
//        ObjectContainer containerExcelReport = report1.writeXlsxReport("/home/user/Рабочий стол");
//        log.info("Открытие сообщений из containerExcelReport");
//        log.info(containerExcelReport.printInfos());
//        if (!containerExcelReport.getWarnings().isEmpty()) {
//            log.info(containerExcelReport.printWarnings());
//        }
//        if (!containerExcelReport.getErrors().isEmpty()) {
//            log.info(containerExcelReport.printErrors());
//        }
        
        
//        log.info("Удаление отчета с id= " + report.getUid());
//        ObjectContainer<Void> containerDelete = reportService.delete(report);
//        ObjectContainer<Void> containerDelete = reportService.delete(report.getUid());
//        log.info(containerDelete.printInfos());
//        if (!containerDelete.getWarnings().isEmpty()) {
//            log.info(containerDelete.printWarnings());
//        }
//        if (!containerDelete.getErrors().isEmpty()) {
//            log.info(containerDelete.printErrors());
//        }
        
        log.info("[" + event.toString() + "] Сервер ReportMicroservice. Старт сервера завершен.");
    }

    /**
     * Метод для заполнения экземпляра класса ReportTest.
     * 
     * @param report экземпляр класса ReportTest
     */
    void createReport(ReportTest report, String name) 
    {
        log.info("Формирование экземпляров класса Field ...");
        List<AbstractField> fields = new LinkedList<>();
        List<AbstractField> fieldHeadings = new LinkedList<>();
        for (int i = 0; i < 8; i++) {
            FieldText header = new FieldText();
            FieldAddressInteger fieldHeaderAddress = new FieldAddressInteger();
            fieldHeaderAddress.setRow(i);
            fieldHeaderAddress.setColumn(0);
            header.setFieldAddress(fieldHeaderAddress);
            header.setName("Name " + i);
            header.setDisplayName("DisplayName " + i);
            header.setValue("Заголовок " + i);
            fieldHeadings.add(header);
            for (int j = 1; j < 5; j++) {
                FieldText field = new FieldText();
                FieldAddressInteger fieldAddress = new FieldAddressInteger();
                fieldAddress.setRow(i);
                fieldAddress.setColumn(j);
                field.setFieldAddress(fieldAddress);
                field.setName("Name " + j);
                field.setDisplayName("DisplayName " + j);
                field.setValue(new String(this.getRandomString(10)));
                fields.add(field);
            }
            for (int j = 6; j < 10; j++) {
                FieldNumberDouble field = new FieldNumberDouble();
                FieldAddressInteger fieldAddress = new FieldAddressInteger();
                fieldAddress.setRow(i);
                fieldAddress.setColumn(j);
                field.setFieldAddress(fieldAddress);
                field.setName("Name " + j);
                field.setDisplayName("DisplayName " + j);
                field.setValue((double) Math.random());
                fields.add(field);
            }
        }

        log.info("Создание экземпляра класса ReportHead ...");
        ReportHead reportHead = new ReportHead();
        FieldText fieldHead = new FieldText();
        fieldHead.setName("Test");
        fieldHead.setDisplayName("Test");
        fieldHead.setValue("Test");
        reportHead.setHeadHeader(fieldHead);

        log.info("Создание экземпляра класса ReportBody ...");
        ReportBody reportBody = new ReportBody();
        FieldText fieldBody = new FieldText();
        fieldBody.setName("Test");
        fieldBody.setDisplayName("Test");
        fieldBody.setValue("Test");
        reportBody.setBodyHeader(fieldBody);
        ReportTable reportTable = new ReportTable();
        reportTable.setColumnHeadings(fieldHeadings);
        reportTable.setRecords(fields);
        reportBody.setTable(reportTable);

        log.info("Создание экземпляра класса ReportFooter ...");
        ReportFooter reportFooter = new ReportFooter();
        FieldText fieldFooter = new FieldText();
        fieldFooter.setName("Test");
        fieldFooter.setDisplayName("Test");
        fieldFooter.setValue("Test");
        reportFooter.setFooterHeader(fieldFooter);

        log.info("Заполнение полей отчета ...");
        report.setName(name);
        report.setReportHead(reportHead);
        report.setReportBody(reportBody);
        report.setReportFooter(reportFooter);

        log.info("Сохранение отчета ReportTest в бд ...");
        ObjectContainer containerSave = reportService.saveReport(report);
        log.info(containerSave.printInfos());
        if (!containerSave.getWarnings().isEmpty()) {
            log.info(containerSave.printWarnings());
        }
        if (!containerSave.getErrors().isEmpty()) {
            log.info(containerSave.printErrors());
        }
    }

    /**
     * 
     * @param length длина строки, требуемой пользователем
     * @return String
     */
    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
    
    
}
