/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vertexprize.es.report.server.graphql;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Query;
import org.jboss.logging.Logger;
import vertexprize.es.core.objectcontainer.ObjectContainer;
import vertexprize.es.core.report.AbstractReport;
import vertexprize.es.core.report.ReportTest;
import vertexprize.es.report.server.controller.ReportController;
import vertexprize.es.report.server.service.ReportService;

/**
 * Класс, который релизует взаимодействие с GraphQl.
 * <br>
 * Содержит экземпляр класса ReportService, при помощи которого происходит взаимодействие с бд,
 * и логгер из пакета org.jboss.logging.Logger.
 * <br>
 * URL-путь - /q/graphql-ui/
 * 
 * @author Safronov.O.V
 */
@GraphQLApi
@ApplicationScoped
public class ReportGraphQlService {
    
    /**
     * Логгер из пакета org.jboss.logging.Logger.
     */
    private static final Logger log = Logger.getLogger(ReportController.class.getName());
    
    /**
     * Экземпляр класса reportService, при помощи которого происходит
     * взаимодествие с базой данных.
     */
    @Inject
    ReportService reportService;
    
    /**
     * Операция query для получения списка всех отчетов.
     * <br><br>
     * Название операции - "Получение полного списка отчетов".
     * <br>
     * Получение списка происходит при помощи транзакции getAllReports.
     * 
     * @return List&lt;AbstractReport&gt;
     */
    @Query("getAllReports")
    @Description("Получение полного списка отчетов")
    public List<AbstractReport> getAllReports() {  
        
        log.info("Поступил запрос на получение полного списка отчетов " + AbstractReport.class.getSimpleName());
        List<AbstractReport> reports = new ArrayList<>();        

        ObjectContainer<List<AbstractReport>> container = reportService.getAllReports();
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }else{
            reports.addAll(container.getValue());
        }
                 
        return reports;        
    }
    
    /**
     * Операция query для получения экземпляра класса AbstractReport.
     * <br><br>
     * Название операции - "Получение отчета".
     * <br>
     * Получение отчета происходит при помощи транзакции getReport.
     * 
     * @param id экземпляр класса UUID
     * @return AbstractReport
     */
    @Query("getReport")
    @Description("Получение отчета")
    public AbstractReport getReport(UUID id) {

        log.info("Поступил запрос на получение отчета c UUID= " + id);

        ObjectContainer<AbstractReport> container = reportService.getReport(id);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }else{
            return container.getValue();
        }

        return null;
    }
    
    /**
     * Операция mutation для удаления записи отчета в базе данных.
     * <br><br>
     * Название операции - "Создание нового отчета".
     * <br>
     * Удаление отчета происходит при помощи транзакции deleteReport.
     * 
     * @param report экземпляр класса AbstractReport
     * @return AbstractReport
     */
    @Mutation("createReport")
    @Description("Создание нового отчета")
    public AbstractReport createReport(ReportTest report) {

        log.info("Поступил запрос на создание в базе данных нового отчета " + report.toString());

        ObjectContainer<Void> container = reportService.saveReport(report);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }
        
        return report;
    }
    
    /**
     * Операция mutation для создания записи отчета в базе данных.
     * <br><br>
     * Название операции - "Удаление отчета".
     * <br>
     * Создание отчета происходит при помощи транзакции saveReport.
     * 
     * @param id экземпляр класса UUID
     * @return UUID
     */
    @Mutation("deleteReport")
    @Description("Удаление отчета")
    public UUID deleteReport(UUID id) {
        
        log.info("Поступил запрос на удаление из базы данных отчета c UUID= " + id);

        ObjectContainer<Void> container = reportService.deleteReport(id);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }
        
        return id;
    }
    
    /**
     * Операция mutation для обновления названия у записи отчета в базе данных.
     * <br><br>
     * Название операции - "Изменение имени отчета".
     * <br>
     * Перед началом выполнения операции проверяется 
     * входной параметр name на равенство null.
     * <br>
     * Если параметр равен null,
     * то при помощи логгера выводится ошибка.
     * <br>
     * Иначае при помощи транзакции getReport из базы данных берется запись отчета.
     * <br>
     * Если запись была найдена,
     * то при помощи транзакции saveReport меняется название записи отчета.
     * 
     * @param id экземпляр класса UUID
     * @param name экземпляр класса String
     * @return AbstractReport
     */
    @Mutation("updateNameReport")
    @Description("Изменение имени отчета")
    public AbstractReport updateNameReport(UUID id, String name) {

        log.info("Поступил запрос на обновление в базе данных отчета c UUID= " + id);
        ObjectContainer<AbstractReport> containerGet = null;
        
        log.info("Проверка входных параметров ...");
        if (name != null) {
            log.info("Проверка прошла успешно ...");
            containerGet = reportService.getReport(id);
            log.info(containerGet.printInfos("Содержимое списка информационных сообщений"));
            if (!containerGet.getWarnings().isEmpty()) {
                log.warn(containerGet.printWarnings("Содержимое списка предупреждений"));
            }
            if (!containerGet.getErrors().isEmpty()) {
                log.error(containerGet.printErrors("Содержимое списка ошибок"));
            }
            if (containerGet.isPresent()) {
                log.info("Изминение имени отчета с " + containerGet.getValue().getName() + " на " + name + " ...");
                AbstractReport report = containerGet.getValue();
                report.setName(name);
                ObjectContainer<Void> container = reportService.saveReport(report);
                log.info(container.printInfos("Содержимое списка информационных сообщений"));
                if (!container.getWarnings().isEmpty()) {
                    log.warn(container.printWarnings("Содержимое списка предупреждений"));
                }
                if (!container.getErrors().isEmpty()) {
                    log.error(container.printErrors("Содержимое списка ошибок"));
                }
            }
        }else{
            log.error("Ошибка выполнения запроса PUT: входной экземпляр " + String.class.getSimpleName() + " пустой");
        }
        
        return containerGet.getValue();
    }
    
}
