/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vertexprize.es.report.server.service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.jboss.logging.Logger;
import vertexprize.es.core.objectcontainer.ObjectContainer;
import vertexprize.es.core.report.AbstractReport;

/**
 * Класс, который используется для взаимодействия с базой данных.
 * <br>
 * Содержит интерфейс Hibernate ORM EntityManager и логгер из пакета org.jboss.logging.Logger.
 *
 * @author Safronov.O.V
 */
@ApplicationScoped
public class ReportService {

    /**
     * Логгер из пакета org.jboss.logging.Logger.
     */
    private static final Logger log = Logger.getLogger(ReportService.class);

    /**
     * Экземпляр класса EntityManager, который содержит в себе персистентный
     * контекст.
     * 
     * В основе JPA лежит понятие контекст персистенции (Persistence Context). 
     * Это место, где живут сущности. 
     * А мы управляем сущностями через EntityManager. 
     * Когда мы выполняем комманду persist, то мы помещаем сущность в контекст. 
     * Точнее, мы говорим EntityManager'у, что это нужно сделать.
     */
    @Inject
    EntityManager em;

    /**
     * Транзакция по получению списка всех экземпляров класса AbstractReport
     * из базы данных.
     * <br><br>
     * Список всех отчетов записывается при помощи запроса createQuery класса
     * EntityManager на языке Java Persistence.
     * <br>
     * Если было поймано исключение, то оно записывается в поле ошибкок экземпляра
     * класса ObjectContainer.
     * <br>
     * После получения списка он проверяется на пустоту, и результат
     * записывается в информационные сообщения экземпляра класса
     * ObjectContainer.
     * <br>
     * Перед завершением транзакции поле ошибок экземпляра класса ObjectContainer
     * проверяется на пустоту, и если оно содержет ошибки, 
     * то в поле информационных сообщений экземпляра класса ObjectContainer 
     * записывается информация об неудачной попытке получения списка. 
     * <br>
     * Иначе в поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об удачной попытке получения списка всех отчетов. 
     * В поле value записывается список всех отчетов,
     * в поле present записывается true.
     *
     * @return ObjectContainer &lt;List&lt;AbstractReport&gt;&gt;
     */
    @Transactional
    public ObjectContainer<List<AbstractReport>> getAllReports() {

        ObjectContainer<List<AbstractReport>> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getAllReports");

        List<AbstractReport> reports = new LinkedList<>();

        container.addInfos("Получение списка всех отчетов " + AbstractReport.class.getSimpleName() + " ...");
        try {
            reports = (List<AbstractReport>) em.createQuery("Select t from " + AbstractReport.class.getSimpleName() + " t").getResultList();
        } catch (Exception e) {
            container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении списка отчетов: " + e.getMessage());
        }
        container.addInfos("Проверка листа отчетов на пустоту ...");
        if (reports.isEmpty()) {
            container.addInfos("Лист отчетов  пуст");
        } else {
            container.addInfos("Получен список отчетов из [" + reports.size() + "] записи/записей");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Получение списка всех отчетов  прошло успешно");
            container.setValue(reports);
            container.setPresent(true);
        } else {
            container.addInfos("Список всех отчетов не был получен");
        }

        return container;
    }

    /**
     * Транзакция по получению экземпляра класса AbstractReport
     * из базы данных.
     * <br><br>
     * Перед операцией по получению отчета 
     * входной параметр id проверяется на равенство null.
     * <br>
     * Если он равен null, то в ObjectContainer записывается ошибка.
     * <br>
     * Для получения отчета используется метод find класса EntityManager.
     * <br>
     * Если было поймано исключение, то оно записывается в поле ошибкок экземпляра
     * класса ObjectContainer.
     * <br>
     * Перед завершением экземпляр класса AbstractReport проверяется на
     * равенство null, и поле ошибок экземпляра класса ObjectContainer
     * проверяется на пустоту.
     * <br>
     * Если один из результатов равен false, то в поле
     * информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об неудачной попытке получения отчета. 
     * <br>
     * Иначе в поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об удачной попытке получения отчета. 
     * В поле value записывается экземпляр класса AbstractReport,
     * в поле present записывается true.
     *
     * @param id экземпляр класса UUID
     * @return ObjectContainer &lt;AbstractReport&gt;
     */
    @Transactional
    public ObjectContainer<AbstractReport> getReport(UUID id) {

        ObjectContainer<AbstractReport> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getReport");

        AbstractReport report = null;

        if (id != null) {
            container.addInfos("Получение отчета с UUID= " + id + " ...");
            try {
                report = em.find(AbstractReport.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении отчета: " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения отчета: входной экземпляр UUID пустой");
        }

        if (report != null && container.getErrors().isEmpty()) {
            container.addInfos("Получен отчет " + report.toString());
            container.addInfos("Получение отчета прошло успешно");
            container.setValue(report);
            container.setPresent(true);
        } else {
            container.addInfos("Отчет не был найден");
        }

        return container;
    }

    /**
     * Транзакция по сохранению экземпляра класса AbstractReport
     * в базе данных.
     * <br><br>
     * Перед операцией по сохранению отчета 
     * входной параметр report проверяется на равенство null.
     * <br>
     * Если он равен null, то в ObjectContainer записывается ошибка.
     * <br>
     * Если поле id экземпляра класса AbstractReport равен null, 
     * то в базе данных создается новая запись отчета, 
     * иначе происходит слияние с существующей.  
     * <br>
     * Для создания новой записи в базе данных 
     * используется метод persist класса EntityManager.
     * <br>
     * После операции по созданию записи проверяется корректность создания записи.
     * <br>
     * Для проверки корректности создания используется тракзакция по получению отчета по id.
     * Информационные сообщения, предупреждения и ошибки, полученные при выполнении транзакции, 
     * записываются в экземпляр класса ObjectContainer.
     * <br>
     * Если поле present экземпляр класса ObjectContainer равно false,
     * то в ObjectContainer записывается ошибка.
     * <br>
     * Для слияния экземпляра класса AbstractReport с записью в базе данных 
     * используется метод merge класса EntityManager.
     * <br>
     * Перед операции по слиянию в базе данных ищется запись с id, равной id экземпляра класса AbstractReport.
     * <br>
     * Если такая запись найдена, то входной экземпляр сливается с записью в базе данных. 
     * <br>
     * Если при поиске записи в базе данных с id, равной id экземпляра класса AbstractReport,
     * запись не была найдена,
     * то в ObjectContainer записывается ошибка.
     * <br>
     * После операции по слиянию проверяется корректность слияния.
     * <br>
     * Если запись в базе данных неравна экземпляру класса AbstractReport,
     * то в ObjectContainer записывается ошибка.
     * <br>
     * Если при выполнении создания или слияния записи было поймано исключение, 
     * то оно записывается в поле ошибкок экземпляра класса ObjectContainer.
     * <br>
     * Перед завершением транзакции поле ошибок экземпляра класса ObjectContainer
     * проверяется на пустоту, и если оно содержет ошибки, 
     * то в поле информационных сообщений экземпляра класса ObjectContainer 
     * записывается информация об неудачной попытке сохранения отчета. 
     * <br>
     * Иначе в поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об удачной попытке сохранения отчета.
     * В поле present записывается true.
     *
     * @param report экземпляр класса AbstractReport
     * @return ObjectContainer &lt;Void&gt;
     */
    @Transactional
    public ObjectContainer<Void> saveReport(AbstractReport report) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции saveReport");
        String nameOperation = "Сохранение/обновление";

        if (report != null) {
            if (report.getUid() == null) {
                container.addInfos("Сохранение нового отчета " + report.toString() + " в базе данных ...");
                nameOperation = "Сохранение";
                try {
                    em.persist(report);
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при сохранении отчета: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности сохранения отчета ...");
                ObjectContainer<AbstractReport> containerGet = this.getReport(report.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (!containerGet.isPresent()) {
                    container.addError("Ошибка сохранения отчета: отчет не был записан в базе данных");
                }
            } else {
                container.addInfos("Поиск отчета в бд ...");
                nameOperation = "Обновление";
                ObjectContainer<AbstractReport> containerGet = this.getReport(report.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {

                    container.addInfos("Обновление отчета в базе данных c " + containerGet.getValue().toString() + " на " + report.toString() + " ...");
                    try {
                        em.merge(report);
                    } catch (Exception e) {
                        container.addError("Ошибка " + e.getClass().getSimpleName() + " при обновлении отчета: " + e.getMessage());
                    }

                    container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                    em.flush();
                    container.addInfos("Очиста контекста персистенции ...");
                    em.clear();

                    container.addInfos("Проверка корректности обновления отчета ...");
                    containerGet = this.getReport(report.getUid());
                    container.addAllInfos(containerGet.getInfos());
                    if (!containerGet.getWarnings().isEmpty()) {
                        container.addAllWarnings(containerGet.getWarnings());
                    }
                    if (!containerGet.getErrors().isEmpty()) {
                        container.addAllErrors(containerGet.getErrors());
                    }
                    container.addInfos("Сравнение отчета, который был в базе данных " + containerGet.getValue().toString() + " с обновляемым " + report.toString() + " ...");
                    if (containerGet.isPresent() && containerGet.getValue().equals(report)) {
                        container.addError("Ошибка обновления отчета: отчет не изменился c " + containerGet.getValue().toString() + " на " + report.toString());
                    }
                } else {
                    container.addError("Ошибка обновления отчета: отчет с UUID=" + report.getUid() + " не существует в базе данных");
                }
            }
        } else {
            container.addError("Ошибка сохранения отчета: входной экземпляр " + AbstractReport.class.getSimpleName() + " пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos(nameOperation + " отчета в базе данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos(nameOperation + " отчета в базе данных не прошло успешно");
        }

        return container;
    }

    /**
     * Транзакция по удалению записи отчета 
     * из базы данных.
     * <br><br>
     * Перед операцией по удалению отчета 
     * входной параметр id проверяется на равенство null.
     * <br>
     * Если он равен null, то в ObjectContainer записывается ошибка.
     * <br>
     * Если неравен null, то при помощи транзакции по получению отчета
     * проверяется наличие в базе данных записи с id, равной id входного параметра.
     * <br>
     * Если запись не находится, то в ObjectContainer записывается ошибка.
     * <br>
     * Иначе при помощи метода remove класса EntityManager
     * запись удаляется из базы данных. 
     * <br>
     * После операции по удалению отчета проверяется корректность удаления.
     * <br>
     * При помощи транзакции по получению отчета проверяется наличие 
     * в базе данных записи с id, равной id входного параметра.
     * <br>
     * Если такая запись находится,
     * то в ObjectContainer записывается ошибка.
     * <br>
     * Перед завершением транзакции поле ошибок экземпляра класса ObjectContainer
     * проверяется на пустоту, и если оно содержет ошибки, 
     * то в поле информационных сообщений экземпляра класса ObjectContainer 
     * записывается информация об неудачной попытке удаления отчета. 
     * <br>
     * Иначе в поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об удачной попытке удаления отчета.
     * В поле present записывается true.
     * 
     * @param id экземпляр класса UUID
     * @return ObjectContainer &lt;Void&gt;
     */
    @Transactional
    public ObjectContainer<Void> deleteReport(UUID id) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции deleteReport для удалению отчета по UUID");

        if (id != null) {
            container.addInfos("Поиск отчета в бд ...");
            ObjectContainer<AbstractReport> containerGet = this.getReport(id);
            container.addAllInfos(containerGet.getInfos());
            if (!containerGet.getWarnings().isEmpty()) {
                container.addAllWarnings(containerGet.getWarnings());
            }
            if (!containerGet.getErrors().isEmpty()) {
                container.addAllErrors(containerGet.getErrors());
            }
            if (containerGet.isPresent()) {
                AbstractReport report = containerGet.getValue();
                container.addInfos("Удаление отчета " + report.toString() + " из базы данных ...");
                try {
                    em.remove(em.contains(report) ? report : em.merge(report));
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при удалении отчета: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности удаления отчета ...");
                containerGet = this.getReport(report.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {
                    container.addError("Ошибка удаления отчета: запись отчета в базе данных не была удаленна");
                }
            } else {
                container.addError("Ошибка удаления отчета: отчет с UUID=" + id + " не существует в базе данных");
            }
        } else {
            container.addError("Ошибка удаления отчета: входной экземпляр UUID пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Удаление отчета из базы данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos("Удаление отчета из базы данных не прошло успешно");
        }

        return container;
    }

    /**
     * Транзакция по удалению записи отчета 
     * из базы данных.
     * <br><br>
     * Перед операцией по сохранению отчета 
     * входной параметр report проверяется на равенство null.
     * <br>
     * Если он равен null, то в ObjectContainer записывается ошибка.
     * <br>
     * Если неравен null, то при помощи транзакции по получению отчета
     * проверяется наличие в базе данных записи с id, равной id входного экземпляра класса AbstractReport.
     * <br>
     * Если запись не находится, то в ObjectContainer записывается ошибка.
     * <br>
     * Иначе при помощи транзакции по удалению отчета по id 
     * запись удаляется из базы данных. 
     * <br>
     * Перед завершением транзакции поле ошибок экземпляра класса ObjectContainer
     * проверяется на пустоту, и если оно не содержет ошибки, 
     * то в поле present записывается true.
     * 
     * @param report экземпляр класса AbstractReport
     * @return ObjectContainer &lt;Void&gt;
     */
    @Transactional
    public ObjectContainer<Void> deleteReport(AbstractReport report) {
        
        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции deleteReport для удалению отчета");

        if (report != null) {
            ObjectContainer<Void> containerDelete = this.deleteReport(report.getUid());
            container.addAllInfos(containerDelete.getInfos());
            if (!containerDelete.getWarnings().isEmpty()) {
                container.addAllWarnings(containerDelete.getWarnings());
            }
            if (!containerDelete.getErrors().isEmpty()) {
                container.addAllErrors(containerDelete.getErrors());
            }
        } else {
            container.addError("Ошибка удаления отчета: входной экземпляр AbstractReport пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.setPresent(true);
        }

        return container;
    }

}
