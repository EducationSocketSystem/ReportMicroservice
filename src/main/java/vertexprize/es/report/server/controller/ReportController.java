/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vertexprize.es.report.server.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import vertexprize.es.core.objectcontainer.ObjectContainer;
import vertexprize.es.core.report.AbstractReport;
import vertexprize.es.core.report.ReportTest;
import vertexprize.es.report.server.service.ReportService;

/**
 * Класс, который релизует взаимодействие с Rest.
 * <br>
 * Содержит экземпляр класса ReportService, при помощи которого происходит взаимодействие с бд,
 * и логгер из пакета org.jboss.logging.Logger.
 * <br>
 * URL-путь - /report
 *
 * @author Safronov.O.V
 */
@Path("/report")
public class ReportController {

    /**
     * Логгер из пакета org.jboss.logging.Logger.
     */
    private static final Logger log = Logger.getLogger(ReportController.class.getName());

    /**
     * Экземпляр класса ReportService, при помощи которого происходит
     * взаимодествие с базой данных.
     */
    @Inject
    ReportService reportService;

    /**
     * Действие GET для получения списка всех отчетов.
     * <br><br>
     * Получение списка происходит при помощи транзакции getAllReports.
     *
     * @return List&lt;AbstractReport&gt; в JSON формате
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<AbstractReport> getAllReports() {

        log.info("Поступил запрос на получение полного списка отчетов " + AbstractReport.class.getSimpleName());
        List<AbstractReport> reports = new ArrayList<>();

        ObjectContainer<List<AbstractReport>> container = reportService.getAllReports();
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }

        return container.getValue();
    }

    /**
     * Действие GET для получения экземпляра класса AbstractReport.
     * <br><br>
     * Получение отчета происходит при помощи транзакции getReport.
     *
     * @param id экземпляр класса UUID в JSON формате
     * @return AbstractReport в JSON формате
     */
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public AbstractReport getReport(@PathParam("id") UUID id) {

        log.info("Поступил запрос на получение отчета c UUID= " + id);

        ObjectContainer<AbstractReport> container = reportService.getReport(id);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }

        return container.getValue();
    }

    /**
     * Действие DELETE для удаления записи отчета в базе данных.
     * <br><br>
     * Удаление отчета происходит при помощи транзакции deleteReport.
     *
     * @param id экземпляр класса UUID в JSON формате
     */
    @Path("/{id}")
    @DELETE
    public void deleteReport(@PathParam("id") UUID id) {

        log.info("Поступил запрос на удаление из базы данных отчета c UUID= " + id);

        ObjectContainer<Void> container = reportService.deleteReport(id);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }
    }

    /**
     * Действие POST для создания записи отчета в базе данных.
     * <br><br>
     * Создание отчета происходит при помощи транзакции saveReport.
     *
     * @param report экземпляр класса ReportTest
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    public void createReport(ReportTest report) {

        log.info("Поступил запрос на создание в базе данных нового отчета " + report.toString());

        ObjectContainer<Void> container = reportService.saveReport(report);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }

    }

    /**
     * Действие PUT для обновления названия у записи отчета в базе данных.
     * <br><br>
     * Перед началом выполнения запроса проверяется 
     * входной параметр name на равенство null.
     * <br>
     * Если параметр равен null,
     * то при помощи логгера выводится ошибка.
     * <br>
     * Иначае при помощи транзакции getReport из базы данных берется запись отчета.
     * <br>
     * Если запись была найдена,
     * то при помощи транзакции saveReport меняется название записи отчета.
     *
     * @param id экземпляр класса UUID
     * @param name экземпляр класса String
     */
    @Path("/{id}/{name}")
    @PUT
    public void updateNameReport(@PathParam("id") UUID id, @PathParam("name") String name) {

        log.info("Поступил запрос на обновление в базе данных отчета c UUID= " + id);

        log.info("Проверка входных параметров ...");
        if (name != null) {
            log.info("Проверка прошла успешно ...");
            log.info("Получение отчета из базы данных ...");
            ObjectContainer<AbstractReport> containerGet = reportService.getReport(id);
            log.info(containerGet.printInfos("Содержимое списка информационных сообщений"));
            if (!containerGet.getWarnings().isEmpty()) {
                log.warn(containerGet.printWarnings("Содержимое списка предупреждений"));
            }
            if (!containerGet.getErrors().isEmpty()) {
                log.error(containerGet.printErrors("Содержимое списка ошибок"));
            }
            if (containerGet.isPresent()) {
                log.info("Изминение имени отчета с " + containerGet.getValue().getName() + " на " + name + " ...");
                AbstractReport report = containerGet.getValue();
                report.setName(name);
                ObjectContainer<Void> container = reportService.saveReport(report);
                log.info(container.printInfos("Содержимое списка информационных сообщений"));
                if (!container.getWarnings().isEmpty()) {
                    log.warn(container.printWarnings("Содержимое списка предупреждений"));
                }
                if (!container.getErrors().isEmpty()) {
                    log.error(container.printErrors("Содержимое списка ошибок"));
                }
            }
        }else{
            log.error("Ошибка выполнения запроса PUT: входной экземпляр " + String.class.getSimpleName() + " пустой");
        }

    }

}
