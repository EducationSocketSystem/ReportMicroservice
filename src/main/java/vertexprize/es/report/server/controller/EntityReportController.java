package vertexprize.es.report.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import org.jboss.logging.Logger;

/**
 * REST контроллер для получения заданий на формирование отчетов
 * 
 * @author vaganovdv
 */
@Path("/report-composer")
public class EntityReportController {
    
     private static final Logger log = Logger.getLogger(EntityReportController.class.getName());
    
     @Inject 
     ObjectMapper jsonMapper;
     
    /**
     * Формирование отчета для списка экземпляров класса c полным путем className
     * 
     * @param className имя класса
     * @param json
     */
    
     /*
    @Consumes(MediaType.APPLICATION_JSON)
    @POST 
    public void composeReport (@PathParam("className") String className, String json)
    {
        log.info("Фомирование отчета для экземпляра класса ["+className+"]");
        log.info("Получен JSON разиером ["+json.getBytes().length+"] байт");
            
    }
    */
    
    
    
    @GET 
    public String  composeReport (@QueryParam("className") String className)
    {
        log.info("Фомирование отчета для экземпляра класса ["+className+"]");
        
        return className;
            
    }
    
    
}
